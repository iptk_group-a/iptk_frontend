package com.example.hiddenseek;

import android.os.Bundle;
import java.util.Arrays;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import com.google.android.gms.tasks.Task;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.hiddenseek.navigation_button.friend.FriendPage;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.firebase.ui.auth.ErrorCodes;
import androidx.annotation.StringRes;

import com.example.hiddenseek.navigation_button.friend.FriendlistFragment;
import com.example.hiddenseek.navigation_button.map.MapFragment;
import com.example.hiddenseek.navigation_button.profil.ProfilFragment;
import com.example.hiddenseek.navigation_button.shop.ShopFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;



public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final int RC_SIGN_IN = 123;
    private static final String TAG = "SignedInActivity";

    private AuthUI authUI = AuthUI.getInstance();

    Fragment mapFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapFragment = new MapFragment();
        BottomNavigationView bottomNavigationView =findViewById(R.id.nav_menu);

        getSupportFragmentManager().beginTransaction().add(R.id.framelayout,mapFragment).commit();
        bottomNavigationView.setOnNavigationItemSelectedListener(listener);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener listener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()){
                case R.id.profil:
                    fragment = new ProfilFragment();
                    break;
                case R.id.shop:
                    fragment = new ShopFragment();
                    break;
                case R.id.friend:
                    fragment = new FriendPage();
                    break;
                default:
                    fragment = mapFragment;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout,fragment).commit();
            return true;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mAuth=FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null){
            // already signed in

        }
        else{
            // not signed in
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.GoogleBuilder().build(),
                                    new AuthUI.IdpConfig.EmailBuilder().build()

                                    ))
                            .build(),
                    RC_SIGN_IN);
        }
    }


    private void showSnackbar(@StringRes int errorMessageRes) {
        Toast.makeText(getApplicationContext(), errorMessageRes, Toast.LENGTH_SHORT).show();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                showSnackbar(R.string.welcome);
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showSnackbar(R.string.sign_in_cancelled);
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection);
                    return;
                }

                showSnackbar(R.string.unknown_error);
                Log.e(TAG, "Sign-in error: ", response.getError());
            }
        }

    }

    public void logout(View v) {
        if (v.getId() == R.id.profil_image) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivityForResult(
                                    AuthUI.getInstance()
                                            .createSignInIntentBuilder()
                                            .setAvailableProviders(Arrays.asList(
                                                    new AuthUI.IdpConfig.GoogleBuilder().build(),
                                                    new AuthUI.IdpConfig.EmailBuilder().build()))
                                            .build(),
                                    RC_SIGN_IN);
                        }
                    });
        }
    }


}
