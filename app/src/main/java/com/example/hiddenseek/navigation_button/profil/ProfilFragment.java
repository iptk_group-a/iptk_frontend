package com.example.hiddenseek.navigation_button.profil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.hiddenseek.MainActivity;
import com.example.hiddenseek.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;

import java.util.ArrayList;

public class ProfilFragment extends Fragment {
    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profil,container,false);
        ImageView imageView = view.findViewById(R.id.profil_image);
        Log.d("TAG1",String.valueOf(imageView));


        ArrayList<User> mData = null;
        Context mContext;
        UserAdapter mAdapter = null;
        ListView list_user;
        mContext = getContext();
        list_user = view.findViewById(R.id.list_view);

        mData = new ArrayList<>();
        mData.add(new User("爸爸说：","你是我儿子吗？"));
        mData.add(new User("爷爷说：","你是我孙子吗？"));
        mData.add(new User("你说：","爸爸我是您儿子！爷爷我是您孙子！"));
        mAdapter = new UserAdapter(mData,mContext);
        Log.d("TAG!",String.valueOf(list_user));
        list_user.setAdapter(mAdapter);
        return view;

    }
}
