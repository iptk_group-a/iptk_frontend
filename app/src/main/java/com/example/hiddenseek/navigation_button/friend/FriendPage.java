package com.example.hiddenseek.navigation_button.friend;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;

import com.example.hiddenseek.DialogActivity;
import com.example.hiddenseek.MainActivity;
import com.example.hiddenseek.R;
import com.example.hiddenseek.navigation_button.profil.ProfilFragment;
import com.example.hiddenseek.navigation_button.shop.ShopFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FriendPage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FriendPage extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FriendPage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment friendPage.
     */
    // TODO: Rename and change types and number of parameters
    public static FriendPage newInstance(String param1, String param2) {
        FriendPage fragment = new FriendPage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_friendpage, container, false);

        ImageButton button = (ImageButton) rootView.findViewById(R.id.friendListButton);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onButtonClicked(v);
            }
        });

        ImageButton button1 = (ImageButton) rootView.findViewById(R.id.imageButton1);
        button1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), DialogActivity.class);
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }
    public void onButtonClicked(View view){
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.friendFragment, new FriendlistFragment(), "NewFragmentTag");
        ft.commit();

        ft.addToBackStack(null);
    }
}