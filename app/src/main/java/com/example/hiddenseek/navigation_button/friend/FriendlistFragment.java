package com.example.hiddenseek.navigation_button.friend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.hiddenseek.R;

public class FriendlistFragment extends Fragment {
    @Nullable

    View rootView;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_friendlist, container, false);

        ImageButton button = (ImageButton) rootView.findViewById(R.id.backtoFPage);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onButtonClicked(v);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    public void onButtonClicked(View view){
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.friendListFragment, new FriendPage(), "NewFragmentTag1");
        ft.commit();

        ft.addToBackStack(null);
    }
}
